**First run a test**

1. log into aws-azure and select Prd profile, you'll need elevated privileges.
2. edit `test-user-ids.json` with the ids you want.
3. Run a test broadcast `AWS_PROFILE=prd node push-broadcast.js test`
4. Run the extract query `user_query.sql` this looks for push enabled customers (excludes users who have not finished)
5. edit `user-ids.json` with the extracted user_ids
6. Run a PROD broadcast `AWS_PROFILE=prd node push-broadcast.js prod`

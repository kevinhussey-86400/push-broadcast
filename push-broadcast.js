const AWS = require('aws-sdk');
const uuid = require('uuid');
const moment = require('moment');
const _ = require('lodash');
const testUsers = require('./test-user-ids.json');
const realUsers = require('./user-ids.json');
const USER_ID_COUNT = 20;

const params = (body, id, i) => ({
  MessageBody: JSON.stringify(body), /* required */
  QueueUrl: 'https://sqs.ap-southeast-2.amazonaws.com/648573517001/prd-blue-cxe-notification.fifo', /* required */
  MessageDeduplicationId: id + '-' + i,
  MessageGroupId: id + '-' + i
});

const genBody = (id, ids) => ({
  notificationId: id,
  correlationId: id,
  userIds: ids,
  tags: [],
  push: {
    title: 'Scheduled maintenance tonight ⚠️',
    body: 'App unavailable for ~3 hours from 12:30am (Sydney time). Card payments and ATM withdrawals not impacted. More info @ status.86400.com.au'
  },
  expiry: moment().add(1, 'day').utc().toISOString()
});

function sendMessage(sqs, notificationId, userIds) {
  //spit the users into groups of 20
  const userArrays = _.chunk(userIds, USER_ID_COUNT);

  for (const [i, userIds] of userArrays.entries()) {
    setTimeout(function() {
      const body = genBody(notificationId, userIds);
      const message = params(body, notificationId, i);
      sqs.sendMessage(message, function(err, data) {
        if (err) console.log(err, err.stack); // an error occurred
        else {
          console.log(`message sent ${message.MessageDeduplicationId} ${JSON.stringify(body)}`);
        }           // successful response
      });
    }, i * 100)
    ;
  }

}

const sqs = new AWS.SQS({
  region: 'ap-southeast-2'
});

let notificationId;
var myArgs = process.argv.slice(2);

switch (myArgs[0]) {
  case 'test':
    console.log(`publishing test broadcast`);
    sendMessage(sqs, `test-scheduled-maintenance-broadcast-${moment().format('YYYY-MM-DD-HH:mm:ss')}`, testUsers.userIds);
    break;
  case 'prod':
    console.log(`publishing production broadcast`);
    sendMessage(sqs, `scheduled-maintenance-broadcast-${moment().format('YYYY-MM-DD-HH:mm:ss')}`, realUsers.userIds);
    break;
  default:
    console.log('usage: push-broadcast [test|prod]');
}











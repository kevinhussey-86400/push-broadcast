SELECT distinct(d.user_id)
FROM prd_cxe.device d
         INNER JOIN prd_cxe.customer c ON c.user_id = d.user_id
WHERE d.deleted is null
  AND d.push_enabled = true;